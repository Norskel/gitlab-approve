# Gitlab Approve

## Endpoint

    /gitlabMergeRequest POST

## Variables

| Nom              | Description         |Example |
| :--------------- |:--------------------|:-------|
|GITLAB_API_URL|Url to your gitlab instance API|https://gitlab.com/api/v4
|IMAGE_URL|Url to your image for validate
|PRIVATE_TOKEN|Gitlab access token
|USERNAME|Gitlab Username
|GITLAB_WEBHOOK_TOKEN|Token for auth the webhook
