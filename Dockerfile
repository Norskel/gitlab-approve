FROM python:3.8-alpine

EXPOSE 5000

ENV GITLAB_API_URL=""
ENV IMAGE_URL=""
ENV PRIVATE_TOKEN=""
ENV USERNAME=""

WORKDIR /app
COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY main.py /app/

CMD ["python","main.py" ]