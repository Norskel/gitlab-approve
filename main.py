import os
import logging
import requests as requests
from flask import Flask, request


GITLAB_API_URL = os.getenv('GITLAB_API_URL', '')
GITLAB_WEBHOOK_TOKEN = os.getenv('GITLAB_WEBHOOK_TOKEN', '87sdfd*s9894dA855')
IMAGE_URL = os.getenv('IMAGE_URL', '')
PRIVATE_TOKEN = os.getenv('PRIVATE_TOKEN', '')
USERNAME = os.getenv('USERNAME', '')

app = Flask(__name__)
app.logger.setLevel(logging.INFO)

def comment(project_id, merge_request_iid):
    app.logger.info("Send comment on project {} on merge request {}".format(project_id, merge_request_iid))
    url = "{}/projects/{}/merge_requests/{}/notes".format(GITLAB_API_URL, project_id, merge_request_iid)
    data = {"body": "![]({})".format(IMAGE_URL)}
    r = requests.post(url, data=data, headers={"PRIVATE-TOKEN": PRIVATE_TOKEN})
    app.logger.debug(r.json())


@app.route('/gitlabMergeRequest', methods=['POST'])
def mergeRequest():
    data = request.json
    if 'X-Gitlab-Token' not in request.headers:
        return "Unauthorized", 401
    if request.headers['X-Gitlab-Token'] != GITLAB_WEBHOOK_TOKEN:
        return "Unauthorized", 401
    if 'action' not in data['object_attributes']:
        return "No action", 200
    if data['object_attributes']['action'] == "approved" and data['user']['username'] == USERNAME:
        comment(data['project']['id'], data['object_attributes']['iid'])
    return "Comment send", 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
